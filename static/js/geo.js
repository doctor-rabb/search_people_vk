var x = document.getElementById("demo");
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert ("Ошибка получения местонахождения!");
            }
        }
        function showPosition(position) {
            try {
                document.getElementById ("l1").value = position.coords.latitude;
                document.getElementById ("l2").value = position.coords.longitude;
            }
            catch (e) {
                alert ("Подтвердите получение Вашего местоположения в появившемся окне.\nЕсли окно не появилось - значит Ваш браузер не поддерживает получение геолокации или Вы не дали разрешение!");
            }
        }