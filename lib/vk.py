class API (object):

    MAIN_URL = 'https://api.vk.com/method/'

    def __init__(self, token):
        self.token = token

    def get_user (self, user_id):
        import requests, json

        to_json = json.loads(requests.get (self.MAIN_URL + 'users.get?user_id=' + str (user_id) + '&fields=photo_200,country').text)

        return {
            'fname' : to_json ['response'][0]['first_name'],
            'lname' : to_json ['response'][0]['last_name'],
            'avatar' : to_json ['response'][0]['photo_200'],
            'uid' : to_json ['response'][0]['uid'],
            'country' : to_json ['response'][0]['country']
        }

    def get_nearby (self, latitude, longitude, radius):
        import requests, json

        to_json = json.loads(requests.get(self.MAIN_URL + 'users.getNearby?access_token=' + self.token + '&latitude=' + latitude + '&longitude=' + longitude + '&radius=' + radius).text)
        return to_json ['response'][1:]



