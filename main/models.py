from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Log (models.Model):
    class Meta:
        db_table = 'logs'

    body = models.TextField ()
    sender = models.IntegerField ()