# coding: utf8

from django.shortcuts import render_to_response
from django.core.context_processors import csrf

# Create your views here.
def main_render (request):

    args = dict ()
    args.update (csrf(request))

    if request.POST:
        from lib.vk import API
        api = API (request.POST.get ('token'))
        users = list ()
        for i in api.get_nearby (request.POST.get ('l1'), request.POST.get ('l2'), request.POST.get ('radius')):
            users.append (api.get_user (str (i ['uid'])))

        args ['users'] = users
        if len (users) == 0:
            args ['err'] = 'Пользователи не найдены!'
        return render_to_response ('main.html', args)

    return render_to_response ('main.html', args)

